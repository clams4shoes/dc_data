#!/usr/bin/env python

import sys
# import json
import requests

from requests.adapters import HTTPAdapter, Retry

'''
    BC Centre On Substance Use DrugSense Data

    Hits DrugSense API, parses JSON, and outputs CSV.
'''

def main(): 
    try:
        session = requests.Session()

        retries = Retry(total=5,
            backoff_factor=1,
            status_forcelist=[500, 502, 503, 504])

        session.mount('https://bccsu-drugsense.onrender.com/', 
            HTTPAdapter(max_retries=retries))

        response = session.get('https://bccsu-drugsense.onrender.com/_dash-layout')
        data = response.json()

        # request = open('test.json', 'r')
        # data = json.load(request)

        records = (data['props']['children'][3]
            ['props']['children'][0]
            ['props']['children'][2]
            ['props']['children']
            ['props']['children'][1]
            ['props']['children'][0]
            ['props']['children'][0]
            ['props']['children'][0]
            ['props']['data'])

        columns = ''

        for column in records[0].keys():
            columns += '"%s",' % column

        print(columns[:-1])

        for record in records:
            line = ''

            for key, value in record.items():
                if key == 'FTIR':
                    if value:
                        ftir = []
                        for entry in value.split(','):
                            ftir.append(entry.lstrip())
                        value = str(ftir)

                line += '"%s",' % value

            print(line[:-1])

        # request.close()

    except Exception as e:
        print(e)

if __name__ == '__main__':
    sys.exit(main())
